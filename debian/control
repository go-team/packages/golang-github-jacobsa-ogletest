Source: golang-github-jacobsa-ogletest
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Michael Stapelberg <stapelberg@debian.org>,
           Tim Potter <tpot@hpe.com>,
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-any,
               golang-github-jacobsa-oglematchers-dev,
               golang-github-jacobsa-oglemock-dev,
               golang-github-jacobsa-reqtrace-dev,
               golang-golang-x-net-dev,
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-jacobsa-ogletest
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-jacobsa-ogletest.git
Homepage: https://github.com/jacobsa/ogletest
XS-Go-Import-Path: github.com/jacobsa/ogletest

Package: golang-github-jacobsa-ogletest-dev
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         golang-github-jacobsa-oglematchers-dev,
         golang-github-jacobsa-oglemock-dev,
         golang-github-jacobsa-reqtrace-dev,
         golang-golang-x-net-dev,
Description: unit testing framework for Go
 ogletest is a unit testing framework for Go with the following features:
 .
  * An extensive and extensible set of matchers for expressing expectations.
  * Automatic failure messages; no need to say
    t.Errorf("Expected %v, got %v"...).
  * Clean, readable output that tells you exactly what you need to know.
  * Built-in support for mocking through the oglemock package.
  * Style and semantics similar to Google Test and Google JS Test.
 .
 It integrates with Go's built-in testing package, so it works with the go test
 command, and even with other types of test within your package. Unlike the
 testing package which offers only basic capabilities for signalling failures,
 it offers ways to express expectations and get nice failure messages
 automatically.
